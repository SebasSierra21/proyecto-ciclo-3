function validar_nombre(nombre){
    var expRegNombre=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;                
       
       if(nombre.length < 4){
          return false;         
       }
       if(nombre.length > 30){
           return false;
       }
       if (!expRegNombre.exec(nombre)){
           return false;
       }  
       
    
}


function validar_contrasenas(x, y){         
    contrasena = x.value;
    validarContrasena = y.value;
    /*Evalua si el campo contraseña esta vacío */
    if (!contrasena){
        /*alert("El campo validar contraseña, no puede estar vacío")*/
        return false
    }
//   /*Valida que la longitud de las constraseñas sea igual*/
   
    
    if(contrasena.length === validarContrasena.length){        
        return true        
    }
    if(contrasena.length !== validarContrasena.length){        
        return false        
    }
    
    
//   /*Valida que las contraseñas sean iguales */
    if(contrasena === validarContrasena){
        return true;
    }
    if(contrasena !== validarContrasena){
        return false;
    }
    
}//

module.exports.validar_nombre = validar_nombre
module.exports.validar_contrasenas = validar_contrasenas
