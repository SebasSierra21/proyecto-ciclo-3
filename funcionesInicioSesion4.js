let registros = [];

function ordenarEdad(edad){         
    if (registros.length < 20) { 
        let datosDegistro = {
            'nombre': document.getElementById("campoNombre"),
            'contrasena': document.getElementById("campoContrasena"),
            'correo': document.getElementById("campoEmail"),
            'confirmacionContrasena': document.getElementById("campoConfirmarContrasena"),
            'telefono': document.getElementById("campoTelefono"),
            'edad': edad
        };
        registros.push(datosDegistro); 
    } else {
        alert("Tamaño máximo del arreglo alcanzado")
    }

    registros.sort((a, b) => b.edad - a.edad);   
    return registros; 
}

function verificarInicioSesion4(correo, contrasena, arreglo) {
    let correos = arreglo.map(a => a.correo);
    let contrasenas = arreglo.map(a => a.contrasena);

    if (correos.includes(correo) && contrasenas.includes(contrasena)) {
        let respuesta;
        //respuesta = prompt("¿En qué año se dio la batalla de Boyacá?");
        return verificarCaptcha(respuesta);          
    } else {
        return false;
    }
}

// Validat captcha
function verificarCaptcha(respuesta){
    // Pregunta captcha: ¿En que año se dío la batalla de boyaca? 
    // Respuesta: 1819
    let contarMayusculas = 0;

    // Validar si se puede convetir a número
    for (let i=0; i < respuesta.length; i++){
        // Validar si la respuesta esta en minuscula
        if (isNaN(respuesta[i]) && respuesta[i] === respuesta[i].toUpperCase()){
            // Acumular el número de letras que aparecen en mayuscula en la respuesta
            contarMayusculas ++;
        }
    }
    // Validar que la variable contarMayusculas este en cero
    if ( contarMayusculas == 0){
        if ( Number(respuesta) === 1819){// Valida que sean igual teniendo en cuenta el tipo de dato
            // Si es favorable permitir al usuario pasar al menu principal
            window.location.href = '/proyecto-ciclo-3/index.html';
            return true;
        }
    }

    return false;
}

module.exports.ordenarEdad = ordenarEdad;
module.exports.verificarInicioSesion4 = verificarInicioSesion4;
module.exports.verificarCaptcha = verificarCaptcha;
