var registros = [];
var objeto = {};

function recibirEdad(){
    let edad;
    edad = prompt("Digite su edad");
    return edad;    
    //ordenarEdad(edad);
    //promedioEdad(registros);
}

function ordenarEdad(edad){
    
    objeto = {
        'nombre': document.getElementById('campoNombre').value, 
        'contrasena': document.getElementById('campoContrasena').value, 
        'correo': document.getElementById('campoCorreo').value, 
        'confirmacioncontrasena': document.getElementById('campoContrasena').value, 
        'telefono': document.getElementById('campoTelefono').value, 
        'edad':edad
    }
      
    if(registros.length<20){
        registros.push(objeto);

        registros.sort(function(a, b) {
            var edadA = new Number(a.edad);
            var edadB = new Number(b.edad);
        
            if (edadA > edadB) return -1;
            if (edadA < edadB) return 1;
            return 0;
        });    
    }else{
        alert("No podemos almacenar más registros");
    }
    //console.log(registros);        
    return registros;
}

function promedioEdad(registros){ 
    let suma = 0;   
    for (let i=0; i < registros.length ; i++){
        suma += Number(registros[i].edad);        
    }
    let promedio = suma /registros.length;
    
    //console.log(promedio);
    
    return promedio;                   
    
}

module.exports.recibirEdad = recibirEdad;
module.exports.ordenarEdad = ordenarEdad;
module.exports.promedioEdad = promedioEdad;